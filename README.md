# LibreNMS (Ubuntu 18.04 Apache)

# Installation walk through 
 
**Created by:** Rex Ng 


**Last update:** 2/8/19

`Alert` Project no longer maintained, as this [project](https://gitlab.com/guna-hk/guna-nms) will cover most of the steps needed

**Purpose:**

After following this guide you shall have a functional NMS with **2 min polling** and **auto discovery** enabled **.**

This guide was created to help GUNA employess ease their future installation, but also helping others who find it difficult to deploy LibreNMS.

**Warning:**

**These instructions assume you are the root user. If you are not, temporarily become a user with root privileges with sudo -s or sudo -i.**

The following procedures may or may not apply to your current set up, please read the output in terminal and make adjustments.



`I am not responsible for any damage of any kind that this walk through may cause.`

`Any non guide related problems should be reported to LibreNMS own repo.`



**To access this guide, users can acess the [wiki page](https://gitlab.com/timescam/guide-for-librenms-ubuntu-18.04-apache/wikis/home)**